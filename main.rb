require_relative 'caim'
require_relative 'row_data'

data = File.read("iris.data")
data_splited = data.split

sl = []
sw = []
pl = []
pw = []

data_splited.each do |row|
    row_d = row.split(',')
    sl << RowData.new(row_d[0].to_f, row_d[4])
    sw << RowData.new(row_d[1].to_f, row_d[4])
    pl << RowData.new(row_d[2].to_f, row_d[4])
    pw << RowData.new(row_d[3].to_f, row_d[4])
end

caim_sl = Caim.new(sl)
caim_sw = Caim.new(sw)
caim_pl = Caim.new(pl)
caim_pw = Caim.new(pw)


puts "Discretización de sepal length"
caim_sl.run_caim
puts "Valores de discretización"
p caim_sl.list_limits
puts "Lista de CAIM final"
p caim_sl.list_caim
puts

puts "Discretización de sepal width"
caim_sw.run_caim
puts "Valores de discretización"
p caim_sw.list_limits
puts "Lista de CAIM final"
p caim_sw.list_caim
puts

puts "Discretización de petal length"
caim_pl.run_caim
puts "Valores de discretización"
p caim_pl.list_limits
puts "Lista de CAIM final"
p caim_pl.list_caim
puts

puts "Discretización de petal width"
caim_pw.run_caim
puts "Valores de discretización"
p caim_pw.list_limits
puts "Lista de CAIM final"
p caim_pw.list_caim

