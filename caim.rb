class Caim
    attr_accessor :data, :d0, :dm, :list_limits, :list_caim, :unique_values, :global
    def initialize(data)
        @data = data
        @d0 = 0.0
        set_d0
        @dm = 0.0
        set_dm
        @list_limits = []
        @list_limits << @d0 << @dm
        @list_caim = []
        @unique_values = []
        set_unique_values
        @global = 0.0
    end

    def set_d0
        min = @data[0].value
        @data.each do |item|
            if item.value < min
                min = item.value
            end
        end
        @d0 = min
    end

    def set_dm
        max = 0.0
        @data.each do |item|
            if item.value > max
                max = item.value
            end
        end
        @dm = max
    end

    def set_unique_values
        @data.each do |item|
            if !@unique_values.include?(item.value)
                @unique_values << item.value
            end
        end
        sort_list(@unique_values)
        @unique_values.delete(@d0)
        @unique_values.delete(@dm)
    end

    def sort_list(list)
        for i in 1..list.length-1
            j = i
            while j > 0 && list[j-1] > list[j]
                aux = list[j]
                list[j] = list[j-1]
                list[j-1] = aux
                j -= 1
            end
        end
    end

    def run_caim
        while !unique_values.empty?
            caim_values = []
            @unique_values.each do |value|
                d = []
                @list_limits.each do |val|
                    d << val
                end
                d << value
                sort_list(d)

                frequencies_table = []

                for i in 1..d.length-1
                    frequencies = count_in_interval(d[i-1], d[i])
                    frequencies_table << frequencies
                end

                caim = calculate_caim(frequencies_table)
                caim_values << [caim, value]
            end
            max_caim = caim_values.max[0]
            value_max_caim = caim_values.max[1]
            @unique_values.delete(value_max_caim)
            if max_caim > @global
                @global = max_caim
                @list_caim << max_caim
                @list_limits << value_max_caim
                sort_list(@list_limits)
            end
        end
    end

    # gets the number of records in a interval per class
    def count_in_interval(val1, val2)
        values_in_interval = []
        @data.each do |item|
            if item.value >= val1 && item.value < val2
                values_in_interval << item
            end
        end
        frequencies_in_interval = [0,0,0]
        values_in_interval.each do |item|
            if item.iris_class == "Iris-setosa"
                frequencies_in_interval[0] += 1
            elsif item.iris_class == "Iris-versicolor"
                frequencies_in_interval[1] += 1
            else
                frequencies_in_interval[2] += 1
            end
        end 
        frequencies_in_interval
    end

    # calculates the caim value of a unique value
    def calculate_caim(frequencies_table)
        summation = 0.0
        frequencies_table.each do |frequencies|
            total = 0
            frequencies.each do |frequencie|
                total += frequencie
            end
            max = frequencies.max
            result = max**2 / total
            summation += result
        end
        caim = summation / frequencies_table.length
        caim
    end
end