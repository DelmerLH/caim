class RowData
    attr_accessor :value, :iris_class
    def initialize(value, cl)
        @value = value
        @iris_class = cl
    end
end